from django.contrib import admin
from .models import Usuario, Donador

# Register your models here.
class UsuarioAdmin(admin.ModelAdmin):
    list_display = ['nombre','contraseña','email']
    search_fields = ['nombre','email']
    list_filter = ['email']
    list_per_page = 10

class DonadorAdmin(admin.ModelAdmin):
    list_display = ['nombres','apellidos','edad','email']
    search_fields = ['nombres','apellidos','edad','email']
    list_filter = ['email']
    list_per_page = 10


admin.site.register(Usuario, UsuarioAdmin)
admin.site.register(Donador, DonadorAdmin)
